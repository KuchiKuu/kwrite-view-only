#!/bin/sh


# THIS SCRIPT MAY DESTROY YOUR SYSTEM 
# USE AT YOUR OWN RISK
# DON'T BLAME ME LATER FOR DAMAGES!!
# YOU HAVE BEEN WARNED!!!!!!!!!!!!!!!


# View text files in view-only mode
# in order to prevent making changes to the original file

# KuchiKuu
# 2021-03-13


# In order to prevent changing the original file
# a copy is created in /tmp/, opened and later removed.
# If no /tmp/ is found, current directory will be used
# If current directory is read-only, /home (~) will be used

# If no -p ($TRIGGER) is used, the script passes all provided arguments
# to kwrite ($EDITOR) program.

# Other programs should also work.

# This is quite a lame approach. Not gonna lie.

# WARNING:  If the script gets terminated, either by closing
#			the terminal, or pressing CTRL+C,
#			then the "view-only" file will not be removed.
#			That's why it is best if /tmp/ exists
#			as it gets cleared at system shutdown.

# WARNING:	This script blindly assumes that:
#			1) AT LEAST /home (~) is writable
#			2) The editor does exist at path provided in $EDITOR
#			3) rm won't remove your whole system or home folder
#				even if * is used, only the first file name
#				is taken into account ($2)


# Usage:
# 	Name this script as the editor that you want to use
# 	view-only in. Add it to PATH, where it will be evaluated
# 	before the original text editor

# Example:
#	1) Put this script in /home/user/.local/bin
#	2) name it kwrite
#	3) add /home/user/.local/bin to PATH if it already isn't
#		 For example: echo "PATH=/home/user/.local/bin:$PATH" >> ~/.bashrc
#		 !!!Remember about ">>". ">" will overwrite your whole .bashrc!!!
#	4) execute kwrite as usual. Use -p to trigger view-only mode.

# Editors with built-in functionality of this script
# > nano -v / nano --view
# > vi -R
# > vim -R


# Set editor's path using one of the options below
#EDITOR=/sbin/kwrite # full path to the editor of choice
EDITOR="/sbin/`basename $0`" # use this script name as editor assuming it's in /sbin/
TRIGGER="-p" # Set custom view-only argument if you like to
# By default, it is set as -p because kwrite has no -p argument
# and -p in this case means "protected"

#echo "Arguments: $@" # uncomment to always see passed arguments
if [[ $1 == $TRIGGER ]]; then
	if [[ $2 ]]; then
		if [[ -f $2 ]]; then
			if [[ !(-r $2) ]]; then
				echo "Can not read the file"
				echo "Quitting..."
				exit
			fi
			FILENAME="`basename $2`-view-only-`date +'%s%N'`"
			if [[ (-d "/tmp") && (-w "/tmp") ]]; then
				FILENAME=/tmp/$FILENAME
			elif [[ -w "`pwd`" ]]; then
				FILENAME="`pwd`/$FILENAME"
			else
				FILENAME=~/$FILENAME
			fi
			echo "File path: $FILENAME" # comment if you don't want to see view-only file's path
			if [[ -f $EDITOR ]]; then
				cp $2 $FILENAME
				$EDITOR $FILENAME
				rm $FILENAME
			else
				echo "Error: Can't find editor"
				echo "Quitting..."
				exit
			fi
		else
			echo -e "Error: Can't open '$2'"
			echo "File creation is not permitted in -p mode"
			echo "Quitting..."
			exit
		fi
	else
		echo "Error: No file name provided."
		echo "Quitting..."
		exit
	fi
else
	$EDITOR $@
fi


#	"In TempleOS, my compiler will put together multiple characters in a character constant.
#	We don't do Unicode. We do ASCII--8-bit ASCII, not 7-bit ASCII; 7-bit signed ASCII is retarded."
#	~Terry Davis
